# TG1

### ¿Cual es el objetivo de este grupo?
Proyecto para llevar a cabo el trabajo en grupo, del grupo 02, de la asignatura Desarrollo con Tecnologías Emergentes de grado Ingeniería en Sistemas de Información en la UAH.

### ¿Quienes componen el grupo?
- Víctor Alba Muñiz
- Lucas Jorge Espejo
- Alejandro Martín Merino
- Daniel Sanz Mena
- Jorge Sánchez Sánchez

### Nuestro trabajo
Puede visitar la web del grupo a través de este enlace: https://dte2021g02.gitlab.io/tg1/tg1.html
